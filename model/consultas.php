<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');

	//Login
	function consultaUsuarioConectado($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT
CASE WHEN TIMESTAMPDIFF(SECOND , TOKEN_WEB_TIME, NOW()) < 300 AND TOKEN_WEB IS NOT NULL AND LENGTH(TOKEN_WEB) > 1  THEN 'SI' ELSE 'NO' END 'CHECK'
FROM USUARIO
WHERE RUT = '{$rut}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuario($rut, $pass){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "' AND PASS = '" . $pass . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Refresh
	function checkUsuarioSinPass($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Ingresa Token Login
	function actualizaTokenLogin($rut, $token){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = '{$token}',
TOKEN_WEB_TIME = NOW()
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Chequeo Token
	function checkToken($token){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT
FROM USUARIO
WHERE TIMESTAMPDIFF(SECOND , TOKEN_WEB_TIME, NOW()) < 300
AND TOKEN_WEB  = '{$token}'
AND TOKEN_WEB <> ''";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Borra Token Login
	function borraTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Areas padres
	function consultaAreasComunesPadre($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT PADRE, NOMBRE, TIPO
FROM AREAWEB
WHERE NOMBRE IN
(
SELECT A.NOMBRE
FROM AREAWEB A
LEFT JOIN PERMISOS P
ON A.IDAREAWEB = P.IDAREAWEB
LEFT JOIN PERFIL  R
ON P.IDPERFIL = R.IDPERFIL
LEFT JOIN USUARIO U
ON R.IDPERFIL = U.IDPERFIL
WHERE A.TIPO = 0
AND U.RUT = '{$rut}'
)
UNION ALL
SELECT DISTINCT PADRE, NOMBRE, TIPO
FROM AREAWEB
WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreasComunes(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB
	WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Destruye token
	function destruyeTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL,
TOKEN_WEB_TIME = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Inmuebles
	function datosInmuebles($fecha){
		$con = conectar();
		if($con != 'No conectado'){
		$sql = "SELECT U.IDUNIDAD, U.FECHAPUBLICACION, U.HORA, T.NOMBRE 'TIPO', U.TIPOOPERACION, U.DIRECCIONCOMUNA,
CONCAT(U.DIRECCIONCALLE, ' N° ', U.DIRECCIONNUMERO) 'DIRECCION', U.DIRECCIONDEPTO 'DEPTO-OFICINA',
U.VALORUF, U.VALORPESOS, U.DESPLIEGUE,
CASE WHEN U.CORREDORA = 0 THEN 'Dueño'
ELSE
CASE WHEN U.CORREDORA = -1 THEN 'No disponible' ELSE 'Corredora' END
END 'VENDEDOR', U.NOMBRECORREDORA, U.CONTACTOTELEFONO, U.CONTACTOEMAIL,
CONCAT(U.MTSUTILES,' / ', U.MTSTERRAZA, ' / ', U.MTSCONSTRUIDOS, ' / ', U.MTSTERRENO) 'MTS',
U.DORMITORIOS, U.BANOS, U.URLFICHA, U.WEB,
CONCAT('<span class=\"fa fa-list-alt\" data-toggle=\"tooltip\" data-placement=\"top\" data-html=\"true\" title=\"HOLAAAAAAAAA\"></span>') 'OBS'
FROM UNIDAD_TOCTOC U LEFT JOIN TIPOPROPIEDAD T
ON U.TIPOPROPIEDAD = T.IDTOCTOC
WHERE U.FECHAPUBLICACION = '" . $fecha . "'
ORDER BY U.FECHAPUBLICACION DESC, HORA DESC";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Inmuebles
	function datosMesAnoInmuebles(){
		$con = conectar();
		if($con != 'No conectado'){
		$sql = "CALL ANOMESINMUEBLES";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosIndicadorPromedio($idUnidad){
		$con = conectar();
		if($con != 'No conectado'){
		$sql = "SELECT U2.VALORPESOS 'VALOR_UNIDAD',
						ROUND(AVG(U.VALORPESOS),0) 'VALOR_PROMEDIO',
						CASE WHEN AVG(U.VALORPESOS) - U2.VALORPESOS > 0 THEN 'BAJO' ELSE 'SOBRE' END 'IN_VALOR',
						ROUND(CASE WHEN AVG(U.VALORPESOS) - U2.VALORPESOS > 0 THEN AVG(U.VALORPESOS) - U2.VALORPESOS ELSE (AVG(U.VALORPESOS) - U2.VALORPESOS)*-1 END,0) 'MONTO',
						ROUND(AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END),2) 'MTS_PROMEDIO',
CASE WHEN AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END) - (CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END) > 0 THEN 'BAJO' ELSE 'SOBRE' END 'IN_MTS',
ROUND(CASE WHEN AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END) - (CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END) > 0
THEN AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END) - (CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END)
ELSE (AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END) - (CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END))*-1 END,2) 'MTS',
ROUND(AVG(U.VALORPESOS)/AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END),2) 'VALOR_PROM_MT2',
CASE WHEN ROUND(AVG(U.VALORPESOS)/AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END),2) - ROUND(U2.VALORPESOS/CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END,2) > 0
THEN 'BAJO' ELSE 'SOBRE' END 'IN_PROM_MTS_VALOR',
CASE WHEN ROUND(AVG(U.VALORPESOS)/AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END),2) - ROUND(U2.VALORPESOS/CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END,2) > 0
THEN ROUND(AVG(U.VALORPESOS)/AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END),2) - ROUND(U2.VALORPESOS/CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END,2)
ELSE (ROUND(AVG(U.VALORPESOS)/AVG(CASE WHEN U.MTSUTILES = 0 THEN U.MTSCONSTRUIDOS ELSE U.MTSUTILES END),2) - ROUND(U2.VALORPESOS/CASE WHEN U2.MTSUTILES = 0 THEN U2.MTSCONSTRUIDOS ELSE U2.MTSUTILES END,2))*-1 END 'PROM_MTS2_SEL',
DATE(DATE_ADD(NOW(), INTERVAL -30 DAY)) 'FECHA_INICIO'
						FROM UNIDAD_TOCTOC U
						LEFT JOIN TIPOPROPIEDAD T
						ON U.TIPOPROPIEDAD = T.IDTIPPROPIEDAD
						LEFT JOIN UNIDAD_TOCTOC U2
						ON U.BANOS = U2.BANOS
						AND U.DORMITORIOS  = U2.DORMITORIOS
						AND U.DIRECCIONCOMUNA  = U2.DIRECCIONCOMUNA
						AND U.TIPOOPERACION = U2.TIPOOPERACION
						LEFT JOIN TIPOPROPIEDAD T2
						ON U2.TIPOPROPIEDAD  = T2.IDTIPPROPIEDAD
						AND T.NOMBRE = T2.NOMBRE
						WHERE U2.IDUNIDAD = '{$idUnidad}'
						AND U.VALORPESOS < 1000000000
						AND U.VALORPESOS/U2.VALORPESOS < 3
						AND T.NOMBRE = T2.NOMBRE
						AND U.FECHAPUBLICACION >= DATE(DATE_ADD(NOW(), INTERVAL -30 DAY))";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}
				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
?>
