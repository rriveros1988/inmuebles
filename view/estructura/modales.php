<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>

<!-- Modal de splash -->
<div id="modalAlertasSplash" class="modal modal-fullscreen fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog" role="document">

    <!-- Modal content-->
    <div class="modal-content-t">
      <div class="modal-body alerta-modal-body">
        <h4 id="textoModalSplash"></h4>
        <button id="buttonAceptarAlertaSplash" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPassSplash" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal de alertas -->
<div id="modalAlertas" class="modal modal-fullscreen fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog modal-dialog-box" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body alerta-modal-body">
        <h6 id="textoModal"></h6>
        <button id="buttonAceptarAlerta" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPass" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal ingreso de personal interno -->
<div id="modalIngresoPersonalInterno" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-dialog-box modal-xl" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 style="color:gray;" id="tituloIngresoPersonalInterno"><span class="fas fa-user-plus"></span>&nbsp;&nbsp;Ingreso personal interno</h5>
      </div>
      <div id="bodyIngresoPersonalInterno" class="modal-body alerta-modal-body" style="overflow-y: scroll;">
        <div class="row" style="text-align: left; margin-bottom: 20pt;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos básicos</label>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">RUN</label>
            <input id="rutIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Apellidos</label>
            <input id="apellidosIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Nombres</label>
            <input id="nombresIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">F. nacimiento</label>
            <input id="fechaNacIngresoPersonalInterno" class="form-control" type="date" value="">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Sexo</label>
						<select id="sexoIngresoPersonalInterno" class="form-control">
							<option value="Mujer">Mujer</option>
							<option value="Hombre">Hombre</option>
            </select>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Estado civil</label>
            <select id="estadoCivilIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Situacion militar</label>
            <select id="sitMilitarIngresoPersonalInterno" class="form-control">
              <option value="Pendiente">Pendiente</option>
              <option value="Al día">Al día</option>
            </select>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Fono</label>
            <input id="telefonoIngresoPersonalInterno" class="form-control input-number" type="text" value="">
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Fono emergencia</label>
            <input id="telefonoEmergenciaIngresoPersonalInterno" class="form-control input-number" type="text" value="">
          </div>
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Contacto emergencia</label>
            <input id="contactoIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Nacionalidad</label>
            <select id="nacionalidadIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Región</label>
            <select disabled id="regionIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Provincia</label>
            <select disabled id="provinciaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Comuna</label>
            <select id="comunaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Dirección</label>
            <input id="direccionIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos educacionales</label>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Nivel</label>
            <select id="nivelEduIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Profesión</label>
            <input id="profesionIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Especialidad</label>
            <input id="especialidadIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos laborales</label>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Centro costo</label>
            <select id="cecoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Servicio o Depto.</label>
            <select id="contratoDeptoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cliente</label>
            <select id="clienteIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Actividad</label>
            <select id="actividadIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Fecha ingreso</label>
            <input id="fechaIngIngresoPersonalInterno" class="form-control" type="date" value="">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Mano de obra</label>
            <select id="clasificacionIngresoPersonalInterno" class="form-control">
							<option value="MOD">MOD</option>
							<option value="MOI">MOI</option>
							<option value="MOE">MOE</option>
            </select>
          </div>
					<div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Cargo</label>
            <input id="cargoIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Nivel funcional</label>
            <select id="nivelFuncionalIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Tipo contrato</label>
            <select id="tipoContratoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Requiere uniforme</label>
            <select id="requiereUniformeIngresoPersonalInterno" class="form-control">
							<option value="SI">Si</option>
							<option value="NO">No</option>
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Talla uniforme</label>
            <input id="tallaIngresoPersonalInterno" class="form-control" type="number" value="30" min="34" max="60">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Posee licencia</label>
            <select id="poseeLicenciaIngresoPersonalInterno" class="form-control">
							<option value="SI">Si</option>
							<option value="NO">No</option>
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Tipo licencia</label>
            <select id="tipoLicenciaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Sucursal</label>
            <select id="sucursalIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Sindicato</label>
            <select id="sindicatoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Tipo jornada</label>
            <select id="tipoJornadaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos salariales</label>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Sueldo base (bruto)</label>
            <input id="sueldoBaseIngresoPersonalInterno" class="form-control input-money" type="text" value="$ 0">
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Forma pago</label>
						<select id="formaPagoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Banco</label>
						<select id="bancoPagoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cuenta banco</label>
            <input id="cuentaBancoIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos previsionales</label>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">AFP</label>
						<select id="afpIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">(%) AFP</label>
            <input disabled id="afpPorcentajeIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">(%) SIS</label>
            <input disabled id="sisPorcentajeIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Salud</label>
						<select id="saludIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cargas fam.</label>
						<select id="cargasIngresoPersonalInterno" class="form-control">
							<option value="SI">Si</option>
							<option value="NO">No</option>
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cant. Cargas</label>
            <input id="cantidadCargasIngresoPersonalInterno" class="form-control" type="number" value="0" min="0">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Plan APV</label>
            <input id="planAPVIngresoPersonalInterno" class="form-control" type="text">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Monto APV</label>
            <input id="montoAPVIngresoPersonalInterno" class="form-control input-money" type="text" value="$ 0">
          </div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button id="guardarIngresoPersonalInterno" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
        <button id="cancelarIngresoPersonalInterno" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
