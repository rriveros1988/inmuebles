/* Funciones de apoyo */
function esconderMenu(){
  $("#logoLinkWeb").hide();
  $("#menu-lateral").css("width","45px");
  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
  $("#logoMenu").css("color","black");
  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
  if($("#sesionActiva").val() == 1){
    $("#lineaMenu").fadeOut();
    $(".contenedor-logos").css("display","none");
    $(".contenedor-logos").find('li').css("display","none");
  }
  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
  $("#logoMenu").fadeIn();
}

function mensajeWssp(){
  setTimeout(function(){
    $('#btnWssp').css("opacity",1);
    $('#btnWssp').fadeIn();
    $('#modalAlertasSplash').modal('hide');
    setTimeout(function(){
      $('#bocadilloWssp').fadeIn();
    },1000);
    setTimeout(function(){
      $('#bocadilloWssp').fadeOut();
    },4000);
    setTimeout(function(){
      $('#btnWssp').css("opacity",0.2);
    },5000);
  },1000);
}

function mensajeWsspSinTexto(){
  $('#modalAlertasSplash').modal('hide');
  setTimeout(function(){
    $('#btnWssp').css("opacity",0.2);
    $('#btnWssp').fadeIn();
  },1000);
}

var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/unidadesCapturadas", {
        controller: "unidadesCapturadasController",
        controllerAs: "vm",
        templateUrl : "view/unidades/unidadesCapturadas.html"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(async function(){
        $('#contenido').fadeIn();
        $('#menu-lateral').fadeIn();
        $('#footer').fadeIn();

        await esconderMenu();
        $("#logoMenu").fadeOut();
        // $('#menu-lateral').hover(function(){
        //     $("#menu-lateral").css("width","200px");
        // },
        // function() {
        //     $("#menu-lateral").css("width","45px");
        // });
        $("#loginSystem").show("slide", {direction: "up"}, 800);
    },1500);
    setTimeout(function(){
      mensajeWssp();
    },2000);
});

app.controller("unidadesCapturadasController", function(){
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(async function(){
      var largo = 0;
      var parametros = {
        "fecha": ''
      };
      await $.ajax({
        url:   'controller/datosAnoMesInmuebles.php',
        type:  'post',
        success:  function (response) {
          var p = jQuery.parseJSON(response);
          if(p.aaData.length > 0){
            var cuerpoAnoMes = '';
            for(var j = 0; j < p.aaData.length; j++){
              cuerpoAnoMes = cuerpoAnoMes  + '<option value="' + p.aaData[j].FECHAPUBLICACION + '">' + p.aaData[j].FECHAPUBLICACION + '</option>';
            }
            $("#inputListadoUnidadesAnoMes").html(cuerpoAnoMes);
            var fecha = $("#inputListadoUnidadesAnoMes").val();
            parametros['fecha'] = fecha;
            largo = Math.trunc(($(window).height() - 380)/25);
          }
        }
      });
      $("#inputListadoUnidadesAnoMes").select2({
          theme: "bootstrap"
      });
      await $('#tablaListadoUnidades').DataTable( {
          ajax: {
              url: "controller/datosUnidades.php",
              type: 'POST',
              data: parametros
          },
          columns: [
            { data: 'IDUNIDAD' },
            { data: 'OBS' , className: "centerDataTable"},
            { data: 'FECHAPUBLICACION', render: $.fn.dataTable.render.moment( 'YYYY-MM-DD', 'DD-MM-YYYY' )},
            { data: 'HORA' },
            { data: 'TIPO' },
            { data: 'TIPOOPERACION' },
            { data: 'DIRECCIONCOMUNA' },
            { data: 'DIRECCION' },
            { data: 'DEPTO-OFICINA' },
            { data: 'VALORUF' , render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
            { data: 'VALORPESOS' , render: $.fn.dataTable.render.number( '.', ',', 0, '' ) },
            { data: 'DESPLIEGUE' , className: "centerDataTable"},
            { data: 'MTS' },
            { data: 'DORMITORIOS' , className: "centerDataTable" },
            { data: 'BANOS' , className: "centerDataTable" },
            { data: 'VENDEDOR' },
            { data: 'NOMBRECORREDORA' },
            { data: 'CONTACTOTELEFONO' },
            { data: 'CONTACTOEMAIL' },
            { data: 'WEB' }
          ],
          // responsive: true,
          buttons: [
            {
                extend: 'excel',
                title: null,
                text: 'Excel'
            },
            {
              extend: 'selectNone',
              text: 'Deseleccionar todos'
            }
          ],
          "columnDefs": [
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
            }
          ],
          "select": {
            style: 'single'
          },
          "scrollX": true,
          "paging": true,
          "ordering": true,
          "scrollCollapse": true,
          // "order": [[ 3, "asc" ]],
          "info":     true,
          "lengthMenu": [[largo], [largo]],
          "dom": 'Bfrtip',
          "language": {
              "zeroRecords": "No tiene personal bajo su cargo",
              "info": "Registro _START_ de _END_ de _TOTAL_",
              "infoEmpty": "No tiene personal bajo su cargo",
              "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente"
              },
              "search": "Buscar: ",
              "select": {
                  "rows": "- %d registros seleccionados"
              },
              "infoFiltered": "(Filtrado de _MAX_ registros)"
          },
          "destroy": true,
          "autoWidth": false,
          "initComplete": function(){
            $('#contenido').fadeIn();
            $('#menu-lateral').fadeIn();
            $('#footer').fadeIn();
            $('#tablaListadoUnidades').DataTable().columns.adjust();
          }
      });
      await esconderMenu();
      mensajeWsspSinTexto();
    },1500);
});
