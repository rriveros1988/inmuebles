//Recarga
$(window).on("load",function(event){
  event.preventDefault();
  event.stopImmediatePropagation();
  var tiempo1 = moment(new Date());
  $(document).mousemove(function(event){
    tiempo2 = moment(new Date());
    if(tiempo2.diff(tiempo1, 'seconds') > 20){
      // console.log(tiempo2.diff(tiempo1, 'seconds'));
      $.ajax({
        url: 'controller/cookie.php',
        type: 'post',
        success: function (response){
        }
      });
      tiempo1 = tiempo2;
    }
	});
  $.ajax({
      url:   'controller/checkToken.php',
      type:  'post',
      success: function (response) {
        if(response === 'TOKEN_NO'){
          $(".contenedor-logos").css("display","none");
          $(".contenedor-logos").find('li').css("display","none");
          window.location.href = "#/home";
          $("#logoLinkWeb").fadeOut();
          $("#logoMenu").fadeOut();
          $("#lineaMenu").fadeOut();
          $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
          $("#menu-lateral").css("width","45px");
          $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
          $("#logoMenu").css("color","black");
          $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
          $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
        }
        else{
          $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success: function (response) {
                var p = jQuery.parseJSON(response);
                var size = Object.size(p.aaData)/2;
                if(size > 0){
                  if(p.aaData['ESTADO'] === 'Activo'){
                    n = p.aaData['NOMBRE'].split(" ");
                    if(n.length <= 3){
                      $("#nombrePerfil").html(p.aaData['NOMBRE']);
                    }
                    else{
                      $("#nombrePerfil").html(n[0] + ' ' + n[2] + ' ' + n[3]);
                    }
                    $.ajax({
                      url:   'controller/datosAreasComunesPadres.php',
                      type:  'post',
                      success: function (response2) {
                        var p2 = jQuery.parseJSON(response2);
                        if(p2.aaData.length !== 0){
                          $(".contenedor-logos").css("display","none");
                          $(".contenedor-logos").find('li').css("display","none");
                          $("#sesionActiva").val("1");
                          $("#sesionActivaUso").val("0");
                          $("#logoMenu").fadeIn();
                        }
                      }
                    });
                  }
                  else{
                    $(".contenedor-logos").css("display","none");
                    $(".contenedor-logos").find('li').css("display","none");
                    window.location.href = "#/home";
                    $("#logoLinkWeb").fadeOut();
                    $("#logoMenu").fadeOut();
                    $("#lineaMenu").fadeOut();
                    $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                    $("#menu-lateral").css("width","45px");
                    $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                    $("#logoMenu").css("color","black");
                    $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                    $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                  }
                }
                else{
                  $(".contenedor-logos").css("display","none");
                  $(".contenedor-logos").find('li').css("display","none");
                  window.location.href = "#/home";
                  $("#logoLinkWeb").fadeOut();
                  $("#logoMenu").fadeOut();
                  $("#lineaMenu").fadeOut();
                  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                  $("#menu-lateral").css("width","45px");
                  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                  $("#logoMenu").css("color","black");
                  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                }
              }
          });
        }
      },
      complete: function(){
        $('#contenido').fadeIn();
        $('#footer').fadeIn();
        $('#menu-lateral').fadeIn();
      }
  });
  setInterval(function(){
    if($("#sesionActiva").val() != "0"){
      $.ajax({
        url:   'controller/checkToken.php',
        type:  'post',
        success: function (response) {
          if(response === 'TOKEN_NO'){
            $.ajax({
                url:   'controller/cerraSesion.php',
                type:  'post',
                success: function (response) {
                  $(".modal").modal("hide");
                  $(".contenedor-logos").css("display","none");
                  $(".contenedor-logos").find('li').css("display","none");
                  $("#sesionActiva").val("0");
                  $("#sesionActivaUso").val("0");
                  window.location.href = "#/home";
                  setTimeout(function(){
                    $("#cerradoInactivo").show();
                  },300);
                  $("#logoLinkWeb").fadeOut();
                  $("#logoMenu").fadeOut();
                  $("#lineaMenu").fadeOut();
                  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                  $("#menu-lateral").css("width","45px");
                  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                  $("#logoMenu").css("color","black");
                  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                }
            });
          }
        }
      });
    }
  },60000);
});

//Login a sistema
$("#loginSystem-submit").unbind('click').click(function(){
    var URLactual = window.location;
    var parametros = {
        "pass" : $("#loginSystem-pass").val(),
        "rut" : $("#loginSystem-rut").val().replace('.','').replace('.',''),
        "url" : URLactual.toString()
    };
    $.ajax({
        data:  parametros,
        url:   'controller/datosUsuarioConectado.php',
        type:  'post',
        beforeSend: function(){
            $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
            $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
            $('#modalAlertasSplash').modal('show');
        },
        success: function (response) {
          var p = jQuery.parseJSON(response);
          if(p.aaData.length !== 0){
            if(p.aaData[0]['CHECK'] == 'NO'){
              $.ajax({
                  data:  parametros,
                  url:   'controller/datosLogin.php',
                  type:  'post',
                  beforeSend: function(){
                      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
                      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
                      $('#modalAlertasSplash').modal('show');
                  },
                  success: function (response) {
                    var p = jQuery.parseJSON(response);
                    var size = Object.size(p.aaData)/2;
                    if(size > 0){
                      if(p.aaData['ESTADO'] === 'Activo'){
                        n = p.aaData['NOMBRE'].split(" ");
                        if(n.length <= 3){
                          $("#nombrePerfil").html(p.aaData['NOMBRE']);
                        }
                        else{
                          $("#nombrePerfil").html(n[0] + ' ' + n[2] + ' ' + n[3]);
                        }
                        $("#sesionActiva").val("1");
                        $("#sesionActivaUso").val("0");
                        $("#logoMenu").fadeIn();
                        window.location.href = "#/unidadesCapturadas";
                      }
                      else{
                        $("#buttonAceptarAlerta").css("display","inline");
                        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                        $("#textoModal").html("El usuario ingresado no se encuentra activo en sistema");
                        setTimeout(function(){
                          $('#modalAlertasSplash').modal('hide');
                        },500);
                        $('#modalAlertas').modal('show');
                      }
                    }
                    else{
                      $("#buttonAceptarAlerta").css("display","inline");
                      $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                      $("#textoModal").html("Datos de acceso incorrectos o no registrados en sistema");
                      setTimeout(function(){
                        $('#modalAlertasSplash').modal('hide');
                      },500);
                      $('#modalAlertas').modal('show');
                    }
                  }
              });
            }
            else{
              $("#buttonAceptarAlerta").css("display","inline");
              $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
              $("#textoModal").html("El usuario ingresado ya se encuentra conectado al sistema");
              setTimeout(function(){
                $('#modalAlertasSplash').modal('hide');
              },500);
              $('#modalAlertas').modal('show');
            }
          }
          else{
            $("#buttonAceptarAlerta").css("display","inline");
            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#textoModal").html("Datos de acceso incorrectos o no registrados en sistema");
            setTimeout(function(){
              $('#modalAlertasSplash').modal('hide');
            },500);
            $('#modalAlertas').modal('show');
          }
        }
    });
});

//Verificar input quita borde rojo
$("#loginSystem-rut").on('input', function(){
  $(this).css("border","");
});

//Verifica rut
function Rut()
{
  var texto = window.document.getElementById("loginSystem-rut").value;
  var tmpstr = "";
  for ( i=0; i < texto.length ; i++ )
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
      tmpstr = tmpstr + texto.charAt(i);
  texto = tmpstr;
  largo = texto.length;

  if(texto == ""){
    return false;
  }
  else if ( largo < 2 )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }

  for (i=0; i < largo ; i++ )
  {
    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
    {
      $("#buttonAceptarAlerta").css("display","inline");
      $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
      $('#modalAlertas').modal('show');
      window.document.getElementById("loginSystem-rut").value = "";
      $("#loginSystem-rut").css("border","1px solid red");
      window.document.getElementById("loginSystem-rut").focus();
      window.document.getElementById("loginSystem-rut").select();
      return false;
    }
  }

  var invertido = "";
  for ( i=(largo-1),j=0; i>=0; i--,j++ )
    invertido = invertido + texto.charAt(i);
  var dtexto = "";
  dtexto = dtexto + invertido.charAt(0);
  dtexto = dtexto + '-';
  cnt = 0;

  for ( i=1,j=2; i<largo; i++,j++ )
  {
    //alert("i=[" + i + "] j=[" + j +"]" );
    if ( cnt == 3 )
    {
      dtexto = dtexto + '.';
      j++;
      dtexto = dtexto + invertido.charAt(i);
      cnt = 1;
    }
    else
    {
      dtexto = dtexto + invertido.charAt(i);
      cnt++;
    }
  }

  invertido = "";
  for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
    invertido = invertido + dtexto.charAt(i);

  window.document.getElementById("loginSystem-rut").value = invertido.toUpperCase()

  if ( revisarDigito(texto) )
    return true;

  return false;
}

function revisarDigito2( dvr )
{
  dv = dvr + ""
  if ( dv != "" && dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar un digito verificador válido");
    $('#modalAlertas').modal('show');
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  return true;
}

function revisarDigito( crut )
{
  largo = crut.length;
  if(crut == ""){
    return false;
  }
  else if ( largo < 2)
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  if ( largo > 2 )
    rut = crut.substring(0, largo - 1);
  else
    rut = crut.charAt(0);
  dv = crut.charAt(largo-1);
  revisarDigito2( dv );

  if ( rut == null || dv == null )
    return 0

  var dvr = '0'
  suma = 0
  mul  = 2

  for (i= rut.length -1 ; i >= 0; i--)
  {
    suma = suma + rut.charAt(i) * mul
    if (mul == 7)
      mul = 2
    else
      mul++
  }
  res = suma % 11
  if (res==1)
    dvr = 'k'
  else if (res==0)
    dvr = '0'
  else
  {
    dvi = 11-res
    dvr = dvi + ""
  }
  if ( dvr != dv.toLowerCase() )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false
  }

  return true
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

//Cerrar sesion
$("#buttonLogout").unbind('click').click(function(){
  $.ajax({
      url:   'controller/cerraSesion.php',
      type:  'post',
      success: function (response) {
        $(".contenedor-logos").css("display","none");
        $(".contenedor-logos").find('li').css("display","none");
        $("#sesionActiva").val("0");
        $("#sesionActivaUso").val("0");
        window.location.href = "#/home";
        $("#logoLinkWeb").fadeOut();
        $("#logoMenu").fadeOut();
        $("#lineaMenu").fadeOut();
        $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
        $("#menu-lateral").css("width","45px");
        $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
        $("#logoMenu").css("color","black");
        $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
        $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
      }
  });
});

$("#inputListadoUnidadesVerURL").unbind('click').click(function(){
  var table = $('#tablaListadoUnidades').DataTable();
  var c = $.map(table.rows('.selected').data(), function (item) {
    return item.URLFICHA;
  });
  window.open(c[0],'_blank');
});

$("#inputListadoUnidadesAnoMes").unbind('click').change(async function(event){
  event.stopImmediatePropagation();
  event.preventDefault();
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  $("#inputListadoUnidadesAnoMes").select2({
      theme: "bootstrap"
  });
  var parametros = {
    "fecha": $("#inputListadoUnidadesAnoMes").val()
  }
  var largo = Math.trunc(($(window).height() - 380)/25);
  await $('#tablaListadoUnidades').DataTable( {
      ajax: {
          url: "controller/datosUnidades.php",
          type: 'POST',
          data: parametros
      },
      columns: [
        { data: 'IDUNIDAD' },
        { data: 'OBS' , className: "centerDataTable"},
        { data: 'FECHAPUBLICACION', render: $.fn.dataTable.render.moment( 'YYYY-MM-DD', 'DD-MM-YYYY' )},
        { data: 'HORA' },
        { data: 'TIPO' },
        { data: 'TIPOOPERACION' },
        { data: 'DIRECCIONCOMUNA' },
        { data: 'DIRECCION' },
        { data: 'DEPTO-OFICINA' },
        { data: 'VALORUF' , render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
        { data: 'VALORPESOS' , render: $.fn.dataTable.render.number( '.', ',', 0, '' ) },
        { data: 'DESPLIEGUE' , className: "centerDataTable"},
        { data: 'MTS' },
        { data: 'DORMITORIOS' , className: "centerDataTable" },
        { data: 'BANOS' , className: "centerDataTable" },
        { data: 'VENDEDOR' },
        { data: 'NOMBRECORREDORA' },
        { data: 'CONTACTOTELEFONO' },
        { data: 'CONTACTOEMAIL' },
        { data: 'WEB' }
      ],
      // responsive: true,
      buttons: [
        {
            extend: 'excel',
            title: null,
            text: 'Excel'
        },
        {
          extend: 'selectNone',
          text: 'Deseleccionar todos'
        }
      ],
      "columnDefs": [
        {
            "targets": [ 3 ],
            "visible": false,
            "searchable": false
        }
      ],
      "select": {
        style: 'single'
      },
      "scrollX": true,
      "paging": true,
      "ordering": true,
      "scrollCollapse": true,
      // "order": [[ 3, "asc" ]],
      "info":     true,
      "lengthMenu": [[largo], [largo]],
      "dom": 'Bfrtip',
      "language": {
          "zeroRecords": "No tiene personal bajo su cargo",
          "info": "Registro _START_ de _END_ de _TOTAL_",
          "infoEmpty": "No tiene personal bajo su cargo",
          "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"
          },
          "search": "Buscar: ",
          "select": {
              "rows": "- %d registros seleccionados"
          },
          "infoFiltered": "(Filtrado de _MAX_ registros)"
      },
      "destroy": true,
      "autoWidth": false,
      "initComplete": function(){
        $('#contenido').fadeIn();
        $('#menu-lateral').fadeIn();
        $('#footer').fadeIn();
        $('#tablaListadoUnidades').DataTable().columns.adjust();
        mensajeWsspSinTexto();
      }
  });
});

// Funciones comunes
// Interaccion con envio de wssp
$("#btnWssp").unbind("click").click(function(){
  if(parseFloat($('#btnWssp').css("opacity")) > parseFloat(0.2)){
    $("#mensajeWssp").fadeOut();
    $('#btnWssp').css("opacity",0.2);
  }
  else{
    $("#bocadilloWssp").hide();
    $("#mensajeWssp").fadeIn();
    $('#btnWssp').css("opacity",1);
  }
});

$("#cierraTextoWssp").unbind("click").click(function(){
  $("#mensajeWssp").fadeOut();
  $('#btnWssp').css("opacity",0.2);
});

$("#btnEnviarTextoWssp").unbind("click").click(function(){
  if($("#textoWssp").val() !== ''){
      window.open("https://api.whatsapp.com/send?phone=56968315567&text=" + $("#textoWssp").val(),'_blank');
      $("#textoWssp").val('');
      $("#mensajeWssp").fadeOut();
  }
  else{
    $("#buttonAceptarAlerta").css("display","inline");
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("Favor ingrese un mensaje a enviar");
    $('#modalAlertas').modal('show');
  }
});

$('.input-number').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
});

$('.input-money').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
});

$('.input-money').unbind("click").change(function(){
  this.value = accounting.formatMoney(this.value, "$ ", 0);
});

function mensajeWsspSinTexto(){
  $('#modalAlertasSplash').modal('hide');
  setTimeout(function(){
    $('#btnWssp').css("opacity",0.2);
    $('#btnWssp').fadeIn();
  },1000);
}
